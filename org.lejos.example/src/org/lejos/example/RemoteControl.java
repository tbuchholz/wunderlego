package org.lejos.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.remote.RemoteMotor;

public class RemoteControl {

    private static final int CODE_START_DRIVING = 123;
    private static final int CODE_DO_TURN       = 1337;
    private static final int CODE_STOP_DRIVING  = 456;

    public static boolean run = true;
    /**
     * @param args
     * @throws IOException
     */

    public static BTConnection c;
    public static DataInputStream in;
    public static DataOutputStream out;

    public static void main(String[] args) throws IOException {
        setUpConnection();
        remoteControl();
        closeConnection();
    }

    public static void setUpConnection() {
        System.out.println("Waiting for incomming connection...");
        c = Bluetooth.waitForConnection();
        in = c.openDataInputStream();
        out = c.openDataOutputStream();

        Motor.B.setSpeed(500);
        Motor.C.setSpeed(500);

        System.out.println("Connection established!");
        System.out.println("waiting for commands");
    }

    public static void stop(){
        run = false;
    }

    public static void remoteControl() throws IOException {
        byte[] b = new byte[12];

        while(run) {
            in.read(b);

            int portA = byteArrayToInt(b, 0);
            int portB = byteArrayToInt(b, 4);
            int portC = byteArrayToInt(b, 8);

            if(portA == 666 && portB == 666 && portC == 666) {
                closeConnection();
            }

//            setMotor(Motor.A, portA);
//            setMotor(Motor.B, portB);
//            setMotor(Motor.C, portC);


//            if(portA > 0){
//                Motor.B.setSpeed(portA);
//                Motor.B.forward();
//            } else if(portA < 0){
//                Motor.B.setSpeed(portA);
//                Motor.B.backward();
//            } else{
//                Motor.B.stop();
//            }
//
//            if(portC > 0){
//                Motor.C.setSpeed(portC);
//                Motor.C.forward();
//            } else if(portC < 0){
//                Motor.C.setSpeed(portC);
//                Motor.C.backward();
//            } else {
//                Motor.C.stop();
//            }

            if(portA == CODE_START_DRIVING) {
                goForward();
            } else if(portA == CODE_DO_TURN) {
                doTurn();
            } else if(portA == CODE_STOP_DRIVING) {
                stopDriving();
            }

            if(Button.ESCAPE.isDown()) {
                stop();
            }
        }
    }

    private static void goForward() {
        Motor.B.forward();
    }

    private static void doTurn() {
        Motor.B.stop();
        sleep(2000);

        Motor.B.backward();
        Motor.C.forward();
        sleep(3000);

        stopDriving();
        sleep(1000);

        Motor.B.forward();
        Motor.C.backward();
        sleep(5000);
        Motor.C.forward();
        sleep(2000);
        stopDriving();
    }

    private static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }

    private static void stopDriving() {
        Motor.B.stop();
        Motor.C.stop();
    }

    public static int byteArrayToInt(byte [] b, int offset) {
        return (b[offset] << 24)
                + ((b[offset+1] & 0xFF) << 16)
                + ((b[offset+2] & 0xFF) << 8)
                + (b[offset+3] & 0xFF);
    }

    public static void setMotor(RemoteMotor motor, int speed){
        if(speed > 0){
            motor.setSpeed(speed);
            motor.forward();
        }
        else if(speed < 0){
            motor.setSpeed(speed);
            motor.backward();
        }else{
            motor.stop();
        }
    }

    public static void closeConnection(){
        try {
            in.close();
            out.close();
        } catch (IOException e) {
        }

        c.close();
        System.exit(0);
    }
}