package de.tarent.android.freedroidz;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class RemoteControlActivity extends Activity {

    private static final int CLOSE = 0;
	private static final int SETTINGS = 1;
    private static final int CODE_START_DRIVING = 123;
    private static final int CODE_DO_TURN       = 1337;
    private static final int CODE_STOP_DRIVING  = 456;


    // accelerometer sensor values.
	private float mAccelX = 0;
	private float mAccelY = 0;
	private float mAccelZ = 0; // this is never used but just in-case future
	// versions make use of it.

	private MotorControl motorControl = new MotorControl();

	private static final double PI = Math.PI;
	protected static final String TAG = "[Freedroidz RemoteControlActivity]";
	
	// http://code.google.com/android/reference/android/hardware/SensorManager.html#SENSOR_ACCELEROMETER
	// for an explanation on the values reported by SENSOR_ACCELEROMETER.
	// TODO: use SensorEventListener, SensorListener is deprecated

	/** sensor manager used to control the accelerometer sensor.*/
	private SensorManager mSensorManager;
	/** acceleration-sensor */
	private Sensor mAccelerometer;
	
	private final SensorEventListener mSensorAccelerometer = new SensorEventListener() {

		// method called whenever new sensor values are reported.
		@Override
		public void onSensorChanged(SensorEvent event) {
//			// grab the values required to respond to user movement.
//
//			// we switched X and Y because we like that way better ;)
//			mAccelX = event.values[1];
//			mAccelY = event.values[0];
//			mAccelZ = event.values[2];
//
//			int[] speeds = calculateMotorSpeed(mAccelX, mAccelY, mAccelZ);
//
//			//Are we supposed to inverse Motors?
//	        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//			final int speedA;
//			final int speedC;
//			if(prefs.getBoolean("preference_inverse_motors", false)) {
//				speedA = speeds[1];
//				speedC = speeds[0];
//			} else {
//				speedA = speeds[0];
//				speedC = speeds[1];
//			}
//			motorControl.setMotorspeedAC(speedA, speedC);
//
//
//			String text = "X-Wert: " + mAccelX + "\n Y-Wert: " + mAccelY
//					+ "\n Z-Wert " + mAccelZ;
//
//			double winkel1 = Math.atan(mAccelX / mAccelY);
//			double winkel2 = Math.atan(mAccelY / mAccelZ);
//			double winkel3 = Math.atan(mAccelX / mAccelZ);
//
//			text += "\n atan(x/y): " + winkel1 + "\n atan(y/z): " + winkel2
//					+ "\n atan(x/z): " + winkel3;
//
//
//			// setContentView(tv);
//		}
//
//		// reports when the accuracy of sensor has change
//		// SENSOR_STATUS_ACCURACY_HIGH = 3
//		// SENSOR_STATUS_ACCURACY_LOW = 1
//		// SENSOR_STATUS_ACCURACY_MEDIUM = 2
//		// SENSOR_STATUS_UNRELIABLE = 0 //calibration required.
        }

        @Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			Log.d(TAG, "Accelerometer-accuracy: " + accuracy);
		}

		
	};
	
	/** calculates the Speed of Left and Right Motor for given acceleration values
	 * 
	 * @param x 
	 * @param y
	 * @param z
	 * @returns new int[] {(int) speedA, (int) speedC};
	 */
	private int[] calculateMotorSpeed(double x, double y, double z) {
		
		
		double winkel1 = (-1) * Math.atan(y / z);
		double winkel2 = Math.atan(x / z);
		
		int maxSpeed = 1500;
		
		// vor-zurück-geschwindigkeit
		double speedA = winkel1/(PI/2) * maxSpeed;
		double speedC = winkel1/(PI/2) * maxSpeed;
		
		// rechts - links 
		if(winkel2 >= 0) {
			speedA -= winkel2/(PI/2) * maxSpeed;
			speedC += winkel2/(PI/2) * maxSpeed;
		} else {
			speedA -= winkel2/(PI/2) * maxSpeed;
			speedC += winkel2/(PI/2) * maxSpeed;
		}
		
//		speedC *= -1; // warum auch immer
		return new int[] {(int) speedA, (int) speedC}; 
		
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.remote_control_view);

		// get Sensor
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


//        initForwardButton();
//        initBackwardButton();
//        initLeftButton();
//        initRightButton();
	}

    private void initForwardButton() {
        final View forwardButton = findViewById(R.id.forward_button);
        forwardButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                handleOnTouchForwardButton(event);
                return false;
            }
        });
    }

    private void handleOnTouchForwardButton(final MotionEvent event) {
        switch(event.getAction()) {
            case KeyEvent.ACTION_DOWN:
                motorControl.setMotorSpeedA(500);
                break;
            case KeyEvent.ACTION_UP:
                motorControl.setMotorSpeedA(0);
                break;
        }
    }

    private void initBackwardButton() {
        final View backwardButton = findViewById(R.id.backward_button);
        backwardButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                handleOnTouchBackwardButton(event);
                return false;
            }
        });
    }

    private void handleOnTouchBackwardButton(final MotionEvent event) {
        switch(event.getAction()) {
            case KeyEvent.ACTION_DOWN:
                motorControl.setMotorSpeedA(-500);
                break;
            case KeyEvent.ACTION_UP:
                motorControl.setMotorSpeedA(0);
                break;
        }
    }

    private void initLeftButton() {
        final View leftButton = findViewById(R.id.left_button);
        leftButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                handleOnTouchLeftButton(event);
                return false;
            }
        });
    }

    private void handleOnTouchLeftButton(final MotionEvent event) {
        switch(event.getAction()) {
            case KeyEvent.ACTION_DOWN:
                motorControl.setMotorSpeedC(-500);
                break;
            case KeyEvent.ACTION_UP:
                motorControl.setMotorSpeedC(0);
                break;
        }
    }

    private void initRightButton() {
        final View rightButton = findViewById(R.id.right_button);
        rightButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                handleOnTouchRightButton(event);
                return false;
            }
        });
    }

    private void handleOnTouchRightButton(final MotionEvent event) {
        switch(event.getAction()) {
            case KeyEvent.ACTION_DOWN:
                motorControl.setMotorSpeedC(500);
                break;
            case KeyEvent.ACTION_UP:
                motorControl.setMotorSpeedC(0);
                break;
        }
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, SETTINGS, 0, getString(R.string.menu_settings));
		menu.add(Menu.NONE, CLOSE, 1, getString(R.string.menu_close));
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case CLOSE:
			
			this.finish();
			return true;
		case SETTINGS: 
			Intent settingsActivity = new Intent(getBaseContext(), PreferenceScreenActivity.class);
			startActivity(settingsActivity);
			return true;
		}
		return false;
	}


	@Override
	protected void onStop() {
		Toast.makeText(this, getString(R.string.remote_control_stopped),Toast.LENGTH_LONG).show();
		
		Freedroidz.closeBTConnection();

		super.onStop();
	}

	@Override
	protected void onResume() {
		// because acceleration-sensor is disabled in onPause() it makes sense to enable it here:
		mSensorManager.registerListener(mSensorAccelerometer, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);

		super.onResume();
	}
	
	@Override
	protected void onPause() {
		// this is the only method which is run in all cases, so we have to disable the acceleration-sensor here
		mSensorManager.unregisterListener(mSensorAccelerometer);
		
		// because acceleration-sensor is disabled, we don't want our robot to move
		motorControl.setMotorspeedABC(0, 0, 0);
		
		super.onPause();
	}

    public void onClickStartDriving(final View view) {
        Freedroidz.sendMessage(new int[] {CODE_START_DRIVING, 0, 0});
    }

    public void onClickDoTurn(final View view) {
        Freedroidz.sendMessage(new int[] {CODE_DO_TURN, 0, 0});
    }

    public void onClickStopDriving(final View view) {
        Freedroidz.sendMessage(new int[] {CODE_STOP_DRIVING, 0, 0});
    }
}
