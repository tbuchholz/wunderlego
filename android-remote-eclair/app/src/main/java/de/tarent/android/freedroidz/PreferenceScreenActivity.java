package de.tarent.android.freedroidz;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class PreferenceScreenActivity extends PreferenceActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		
	}

}
