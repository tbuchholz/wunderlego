package de.tarent.android.freedroidz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import android.util.Log;

public class SensorDataHandler {


    private static final String TAG = "freedroidz - SensorDataHandler";
   
    /**
     * This method does not block.
     * 
     * @param data
     */
    public void incomingData(String data) {
	Runnable dataSender = new DataRunnable(data);
	
	new Thread(dataSender).start();

    }

    private class DataRunnable implements Runnable {
	private static final String url = "http://m2mcontest.eclipsecon.org/REST/sensors/data/_insert";

	private final String json;

	public DataRunnable(String json) {
	    this.json = json;
	}

	@Override
	public void run() {

	    // check if data is at least similiar to a json string
	    if (!(json.startsWith("[{") || json.endsWith("}]"))) {
		String msg = "Received weird SensorData: " + json;
		Log.d(TAG, msg);
		return;
	    }

	    try {
		HttpParams params = new BasicHttpParams();
		params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpClient httpclient = new DefaultHttpClient(params);
		HttpPost httppost = new HttpPost(url);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("docs", json));
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
		HttpResponse response = httpclient.execute(httppost);
		Log.i(TAG, "Status: " + response.getStatusLine().getStatusCode());
	    } catch (IOException e) {
		Log.e(TAG, "Failed to send data: " + e.getMessage());
	    }

	}
    }
}
