package com.android.legobar;

import android.util.Log;

import java.io.IOException;
import java.net.*;
import java.util.Enumeration;

/**
 * User: mirko @ PressMatrix GmbH
 * Date: 06.06.15 | Time: 11:45
 */
public class ServerThread implements Runnable {

    public static final int PORT = 8080;

    private final String mIp;
    private ServerSocket mServerSocket;

    public ServerThread(){
        mIp = getLocalIpAddress();
    }

    public void run() {
        try {

            mServerSocket = new ServerSocket(PORT);

            Log.d("DEBUG", " :: run :: server started at " + mServerSocket.getLocalSocketAddress().toString());
            while (true) {
                // LISTEN FOR INCOMING CLIENTS
                Socket client = mServerSocket.accept();
                Log.d("DEBUG", " :: run :: Client connected");
                //TODO post connected

                Thread threadHandler = new Thread(new TextServerHandler(client));
                threadHandler.start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("ServerActivity", ex.toString());
        }
        return null;
    }

    public void stop(){
        try {
            // MAKE SURE YOU CLOSE THE SOCKET UPON EXITING
            mServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
