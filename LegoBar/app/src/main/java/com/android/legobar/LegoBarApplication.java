package com.android.legobar;

import android.app.Application;
import io.relayr.RelayrSdk;

/**
 * User: mirko @ PressMatrix GmbH
 * Date: 06.06.15 | Time: 14:09
 */
public class LegoBarApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new RelayrSdk.Builder(this).inMockMode(false).build();
    }
}
