package com.android.legobar;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * User: mirko @ PressMatrix GmbH
 * Date: 06.06.15 | Time: 11:33
 */
public class TextServerHandler implements Runnable {
    private Socket clientSocket;

    public TextServerHandler(Socket _clientSocket) {
        this.clientSocket = _clientSocket;
    }

    @Override
    public void run() {
        try {
            Log.d("DEBUG", " :: run :: Thread mit ID " + Thread.currentThread().getId() + " für Clientanfrage gestartet !");
            PrintWriter out = new PrintWriter(this.clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String strInput = null;
            Log.d("DEBUG", " :: run :: Thread " + Thread.currentThread().getId() + " für Clientanfrage wartet auf Antwort...");
            while ((strInput = in.readLine()) != null) {
                Log.d("DEBUG", " :: run :: Thread " + Thread.currentThread().getId() + ": Eingabe: " + strInput);
                out.println(strInput.toUpperCase());
            }

            Log.d("DEBUG", " :: run :: Thread " + Thread.currentThread().getId() + " ist fertig!");
        }
        catch (IOException ioEx) {
            System.out.println("Fehler beim Schreiben:" + ioEx.getMessage());
        }
    }
}
