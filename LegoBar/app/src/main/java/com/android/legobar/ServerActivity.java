package com.android.legobar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


public class ServerActivity extends AppCompatActivity {

    private TextView mServerStatus;
    private ServerThread mServerThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mServerStatus = (TextView) findViewById(R.id.server_state);

        mServerThread = new ServerThread();
        Thread fst = new Thread(mServerThread);
        fst.start();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mServerThread.stop();
    }


}
