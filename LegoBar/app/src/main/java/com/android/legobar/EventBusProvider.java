package com.android.legobar;

import com.squareup.otto.Bus;

/**
 * User: mirkohaberlin @ Homami Mobile UG
 * Date: 04.11.13 | Time: 11:07
 */
public class EventBusProvider {
    private static Bus mBus;

    public static Bus getInstance() {
        if(mBus == null) {
            mBus = new Bus();
        }
        return mBus;
    }
}
