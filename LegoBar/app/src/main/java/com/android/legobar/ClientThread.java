package com.android.legobar;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * User: mirko @ PressMatrix GmbH
 * Date: 06.06.15 | Time: 12:44
 */
public class ClientThread implements Runnable {

    private final String mIp;
    private boolean mIsConnected;

    public ClientThread(final String ip){
        mIp = ip;
    }

    public void run() {
        try {
            Log.d("ClientActivity", "C: Connecting...");
            Socket socket = new Socket("localhost", ServerThread.PORT);
            mIsConnected = true;
            while (mIsConnected) {
                try {
                    Log.d("ClientActivity", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                    // WHERE YOU ISSUE THE COMMANDS
                    out.println("Hey Server!");
                    Log.d("ClientActivity", "C: Sent.");
                } catch (Exception e) {
                    Log.e("ClientActivity", "S: Error", e);
                }
            }
            socket.close();
            Log.d("ClientActivity", "C: Closed.");
        } catch (Exception e) {
            Log.e("ClientActivity", "C: Error", e);
            mIsConnected = false;
        }
    }

    public boolean isConnected(){
        return mIsConnected;
    }
}
