package com.android.legobar;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * User: mirko @ PressMatrix GmbH
 * Date: 06.06.15 | Time: 13:42
 */
public class SocketClientService extends IntentService {

    public SocketClientService() {
        super("SocketClientService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Log.d("ClientActivity", "C: Connecting...");
            Socket socket = new Socket("localhost", ServerThread.PORT);
            Log.d("ClientActivity", "C: Sending command.");

            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            // WHERE YOU ISSUE THE COMMANDS
            out.println("Hey Server!");

            Log.d("ClientActivity", "C: Sent.");
            socket.close();
            Log.d("ClientActivity", "C: Closed.");
        } catch (Exception e) {
            Log.e("ClientActivity", "C: Error", e);
        }
    }
}
