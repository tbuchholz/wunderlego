package com.android.legobar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.internal.LinkedTreeMap;
import io.relayr.RelayrSdk;
import io.relayr.model.*;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mirko @ PressMatrix GmbH
 * Date: 06.06.15 | Time: 12:39
 */
public class ClientActivity extends Activity {

    private TextView mWelcomeTextView;
    private TextView mAccelX;
    private TextView mAccelY;
    private TextView mAccelZ;
    private TransmitterDevice mDevice;
    private Subscription mUserInfoSubscription = Subscriptions.empty();
    private Subscription mTemperatureDeviceSubscription = Subscriptions.empty();
    private Intent mServiceIntent;
    private long mLastCurrentMillis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_activity);

        mWelcomeTextView = (TextView) findViewById(R.id.welcome);
        mAccelX = (TextView) findViewById(R.id.accel_x);
        mAccelY = (TextView) findViewById(R.id.accel_y);
        mAccelZ = (TextView) findViewById(R.id.accel_z);

        mServiceIntent = new Intent();

        if (RelayrSdk.isUserLoggedIn()) {
            updateUiForALoggedInUser();
        } else {
            updateUiForANonLoggedInUser();
            logIn();
        }
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        menu.clear();
//
//        if (RelayrSdk.isUserLoggedIn()) {
//            getMenuInflater().inflate(R.menu.thermometer_demo_logged_in, menu);
//        } else {
//            getMenuInflater().inflate(R.menu.thermometer_demo_not_logged_in, menu);
//        }
//
//        return super.onPrepareOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        if (item.getItemId() == R.id.action_log_in) {
//            logIn();
//            return true;
//        } else if (item.getItemId() == R.id.action_log_out) {
//            logOut();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void logIn() {
        RelayrSdk.logIn(this)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(ClientActivity.this, R.string.unsuccessfully_logged_in, Toast.LENGTH_SHORT).show();
                        updateUiForANonLoggedInUser();
                    }

                    @Override
                    public void onNext(User user) {
                        Toast.makeText(ClientActivity.this, R.string.successfully_logged_in, Toast.LENGTH_SHORT).show();
                        invalidateOptionsMenu();
                        updateUiForALoggedInUser();
                    }
                });
    }

    private void logOut() {
        unSubscribeToUpdates();
        RelayrSdk.logOut();
        invalidateOptionsMenu();
        Toast.makeText(this, R.string.successfully_logged_out, Toast.LENGTH_SHORT).show();
        updateUiForANonLoggedInUser();
    }

    private void updateUiForANonLoggedInUser() {
        mAccelX.setVisibility(View.GONE);
        mAccelY.setVisibility(View.GONE);
        mAccelZ.setVisibility(View.GONE);
        mWelcomeTextView.setText("Halli Hallo");
    }

    private void updateUiForALoggedInUser() {
        mAccelX.setVisibility(View.VISIBLE);
        mAccelY.setVisibility(View.VISIBLE);
        mAccelZ.setVisibility(View.VISIBLE);
        loadUserInfo();
    }

    private void loadUserInfo() {
        mUserInfoSubscription = RelayrSdk.getRelayrApi().getUserInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(ClientActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(User user) {
                        String hello = String.format(getString(R.string.hello), user.getName());
                        mWelcomeTextView.setText(hello);
                        loadAccelerometerDevice(user);
                    }
                });

    }

    private void loadAccelerometerDevice(User user) {
        mTemperatureDeviceSubscription = RelayrSdk.getRelayrApi()
                .getTransmitters(user.id)
                .flatMap(new Func1<List<Transmitter>, Observable<List<TransmitterDevice>>>() {
                    @Override
                    public Observable<List<TransmitterDevice>> call(List<Transmitter> transmitters) {
                        // This is a naive implementation. Users may own many WunderBars or other
                        // kinds of transmitter.
                        if (transmitters.isEmpty())
                            return Observable.from(new ArrayList<List<TransmitterDevice>>());
                        return RelayrSdk.getRelayrApi().getTransmitterDevices(transmitters.get(3).id);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<TransmitterDevice>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(ClientActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<TransmitterDevice> devices) {
                        for (TransmitterDevice device : devices) {
//                            if (device.model.equals(DeviceModel.ACCELEROMETER_GYROSCOPE.getId())) {
//                                Log.d("DEBUG", " :: onNext ::  subscribed to Accelerometer");
//                                subscribeForAccelerationUpdates(device);
//                                return;
//                            } else
                            if(device.model.equals(DeviceModel.LIGHT_PROX_COLOR.getId())){
                                Log.d("DEBUG", " :: onNext ::  subscribed to Accelerometer");
                                subscribeForLightUpdates(device);
                                return;
                            }
                        }
                    }
                });
    }

    private void subscribeForLightUpdates(TransmitterDevice device) {
        mDevice = device;
        RelayrSdk.getWebSocketClient().subscribe(device)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Reading>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(ClientActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Reading reading) {
                        if (reading.meaning.equals("color")) {
                            mAccelX.setText(String.valueOf(((LinkedTreeMap) reading.value).get("red")));
                            mAccelY.setText(String.valueOf(((LinkedTreeMap) reading.value).get("green")));
                            mAccelZ.setText(String.valueOf(((LinkedTreeMap) reading.value).get("blue")));

                            if(isGroundWhite(reading)){
//                                startService(mServiceIntent);

                                final long currentMillis = System.currentTimeMillis();
                                if(currentMillis - mLastCurrentMillis > 10000) {
                                    mServiceIntent.setAction("white_color_detected");
                                    sendBroadcast(mServiceIntent);
                                    mLastCurrentMillis = currentMillis;
                                    Log.d("DEBUG", " :: onNext :: broadcast send");

                                }

                            }
//                            else if(isGround(reading)){
//                                Log.d("DEBUG", " :: onNext :: BODEN!!");
//                            }
                        }
                    }
                });
    }

    private boolean isGroundWhite(Reading reading) {
        return Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("red"))) > 200.0f
                && Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("green"))) > 100.0f
                && Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("blue"))) > 100.0f;
    }

    private boolean isGround(Reading reading) {
        return Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("red"))) < 200.0f && Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("red"))) > 1.0f
                && Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("green"))) < 100.0f && Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("green"))) > 1.0f
                && Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("blue"))) < 100.0f && Float.parseFloat(String.valueOf(((LinkedTreeMap) reading.value).get("blue"))) > 1.0f;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unSubscribeToUpdates();
    }

    private void unSubscribeToUpdates() {
        if (!mUserInfoSubscription.isUnsubscribed())
            mUserInfoSubscription.unsubscribe();

        if (!mTemperatureDeviceSubscription.isUnsubscribed())
            mTemperatureDeviceSubscription.unsubscribe();

        if (mDevice != null)
            RelayrSdk.getWebSocketClient().unSubscribe(mDevice.id);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (RelayrSdk.isUserLoggedIn()) {
            updateUiForALoggedInUser();
        } else {
            updateUiForANonLoggedInUser();
        }
    }

    private void subscribeForAccelerationUpdates(TransmitterDevice device) {
        mDevice = device;
        RelayrSdk.getWebSocketClient().subscribe(device)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Reading>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(ClientActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Reading reading) {
                        if (reading.meaning.equals("acceleration")) {
                            mAccelX.setText(String.valueOf(((LinkedTreeMap) reading.value).get("x")));
                            mAccelY.setText(String.valueOf(((LinkedTreeMap) reading.value).get("y")));
                            mAccelZ.setText(String.valueOf(((LinkedTreeMap) reading.value).get("z")));
                        }
                    }
                });
    }
}
